# Email Signature Generator

## Overview
This Python script automates the process of generating and setting personalized email signatures for users within a Windows environment. It leverages the `win32com.client` to interact with Windows COM objects for retrieving user information from Active Directory and setting the signature in Microsoft Word. The script converts specified images and fonts to Base64 format, integrates them into an HTML signature template, and sets the generated signature as the default for new emails and replies.

## Features
- Retrieves user information from Active Directory
- Converts specified logo and font files to Base64 encoding
- Customizes an HTML signature template with user details
- Sets the generated signature as the default in Microsoft Word

## Prerequisites
- Python 3.x
- `win32com.client` module
- Access to a Windows environment with Active Directory
- Microsoft Word installed

## Installation
1. Ensure Python 3.x is installed on your system.
2. Install the required Python module:
    ```bash
    pip install -r requirements.txt
    ```
3. Clone this repository or download the script to your local machine.

## Usage

1. Update the script with the appropriate file paths for the font, logo, and HTML signature template.
2. Ensure the HTML template contains placeholders for the dynamic content (e.g., `$font`, `$logo`, `$name`, `$title`, etc.).
3. Run the script in a command prompt or terminal:
    ```bash
    python main.py.py
    ```
4. The script will retrieve the current user's information, generate a personalized signature, and set it as the default in Microsoft Word.

## Converting to Executable

To distribute your script as a standalone executable file on Windows, you can use `PyInstaller`. This allows users to run your script without needing to install Python or any dependencies.

### Prerequisites for Conversion
- Install `PyInstaller`:
    ```bash
    pip install -r requirements.dev.txt
    ```

### Conversion Steps
1. Open a command prompt or terminal in the directory containing your script.
2. Run the following command to create the executable:
    ```bash
    pyinstaller --noconfirm --onefile --console --name "signature" --hide-console "minimize-early" "main.py"
    ```
    - `--noconfirm`: Overwrites any existing files created in previous runs.
    - `--onefile`: Bundles everything into a single executable file for simplicity.
    - `--console`: This script runs in a console window.
    - `--name "signature"`: Sets the name of the output executable to `signature.exe`.
    - `--hide-console`: Hides the console window when running the GUI (if applicable).
    - `main.py`: The path to your main Python script.

3. After the process completes, find your `.exe` file in the `dist` folder within your script's directory.

### Usage of the Executable
- Distribute the `.exe` file to the end-users.
- Users can simply double-click the executable to run the script without any additional setup.

### Notes
- The first run of the executable might take a little longer as it extracts the required files.
- If your script uses external files (e.g., templates, images), ensure they are accessible to the executable, possibly by including them in the same directory.


## Customization
- Modify the HTML signature template to fit your design requirements.
- Update the file paths for the logo and font files as needed.

## Troubleshooting
- Ensure that the script is run in a Windows environment with access to Active Directory and Microsoft Word.
- Check the file paths to ensure they point to valid files on your system.
- Verify that the HTML template uses the correct placeholders for dynamic content.

## License
[MIT License](LICENSE)

## Disclaimer
This script is provided as-is with no warranties. It is intended for use in a controlled environment and should be tested thoroughly before being deployed in a production environment.