import os
import win32com.client
import base64
from string import Template

def convert_image_to_base64(image_path):
    """
    Converts an image file to a Base64-encoded string.
    
    Parameters:
        image_path (str): The path to the image file.
        
    Returns:
        str: A Base64-encoded string representing the image.
    """
    with open(image_path, 'rb') as image_file:
        image_bytes = image_file.read()
    image_base64 = base64.b64encode(image_bytes).decode('utf-8')
    return f"data:image/png;base64,{image_base64}"

def convert_font_to_base64(font_path):
    """
    Converts a font file to a Base64-encoded string.
    
    Parameters:
        font_path (str): The path to the font file.
        
    Returns:
        str: A Base64-encoded string representing the font.
    """
    with open(font_path, 'rb') as font_file:
        font_bytes = font_file.read()
    font_base64 = base64.b64encode(font_bytes).decode('utf-8')
    return f"data:font/truetype;charset=utf-8;base64,{font_base64}"

def get_user_info():
    """
    Retrieves the current user's Active Directory information using COM objects.
    
    Returns:
        object: A COM object containing the user's Active Directory information.
    """
    obj_sys_info = win32com.client.Dispatch("ADSystemInfo")
    current_user = obj_sys_info.UserName
    obj_user = win32com.client.GetObject("LDAP://" + current_user)
    return obj_user

def create_signature(
        user_info, 
        signature_template_path, 
        signature_file_path, 
        font_path, 
        logo_path,
        email_icon_path,
        phone_icon_path,
        location_icon_path,
        website_icon_path,
        disclaimer_path ):
    """
    Generates an email signature using provided user information and resources, then saves it to a specified path.
    
    Parameters:
        user_info (object): A COM object containing the user's Active Directory information.
        signature_template_path (str): Path to the HTML template for the signature.
        signature_file_path (str): Destination path for the generated signature file.
        font_path (str): Path to the custom font file.
        logo_path (str): Path to the logo image.
        email_icon_path (str): Path to the email icon.
        phone_icon_path (str): Path to the phone icon.
        location_icon_path (str): Path to the location icon.
        website_icon_path (str): Path to the website icon.
    """
    display_name = getattr(user_info, 'displayName', None)
    given_name, surname = None, None
    
    if display_name:
        parts = display_name.split()
        given_name, surname = parts[0], parts[-1]

    title = getattr(user_info, 'title', None)
    city = getattr(user_info, 'l', None)
    state = getattr(user_info, 'st', None)
    email = getattr(user_info, 'mail', None)
    website = getattr(user_info, 'wWWHomePage', None)
    phone = getattr(user_info, 'telephoneNumber', None)
    
    # Convert provided resources to Base64
    logo = convert_image_to_base64(logo_path)
    font = convert_font_to_base64(font_path)
    email_icon = convert_image_to_base64(email_icon_path)
    phone_icon = convert_image_to_base64(phone_icon_path)
    location_icon = convert_image_to_base64(location_icon_path)
    website_icon = convert_image_to_base64(website_icon_path)

    disclaimer_text = None
    with open(disclaimer_path, 'r', encoding='utf-8') as disclaimer:
        disclaimer_text = disclaimer.read()

    # Read the signature template
    with open(signature_template_path, "r", encoding='utf-8') as signature_template:
        template_content = Template(signature_template.read())

    # Replace placeholders with actual values
    template_content = template_content.safe_substitute(
        font=f'{font}',
        logo=f'{logo}',
        name=f'{given_name} {surname}',
        title=f'{title}',
        email_icon=f'{email_icon}',
        email=f'{email}',
        phone_icon=f'{phone_icon}',
        phone=f'{phone}',
        location_icon=f'{location_icon}',
        location=f'{city}-{state}',
        website_icon=f'{website_icon}',
        website=f'{website}',
        disclaimer=f'{disclaimer_text}'
    )

    with open(signature_file_path, "w", encoding="utf-8") as signature:
        signature.write(template_content)

def set_signature_in_word(signature_name):
    """
    Sets the generated signature as the default signature for new emails and replies in Microsoft Outlook.
    
    Parameters:
        signature_name (str): The name of the signature.
    """

    word_app = win32com.client.Dispatch("Word.Application")
    email_signature = word_app.EmailOptions.EmailSignature
    email_signature.NewMessageSignature = signature_name
    email_signature.ReplyMessageSignature = signature_name
    word_app.Quit()

def main():

    try:

        SIGNATURE_NAME = 'Signature'
        BASE_PATH = r'Path to the folder with all assets '
        
        FONT_PATH = os.path.abspath(os.path.join(BASE_PATH, "fonts/font.ttf"))
        LOGO_PATH = os.path.abspath(os.path.join(BASE_PATH, "images/logo.png"))
        EMAIL_ICON_PATH = os.path.abspath(os.path.join(BASE_PATH, "images/email_icon.png"))
        PHONE_ICON_PATH = os.path.abspath(os.path.join(BASE_PATH, "images/phone_icon.png"))
        LOCATION_ICON_PATH = os.path.abspath(os.path.join(BASE_PATH, "images/location_icon.png"))
        WEBSITE_ICON_PATH = os.path.abspath(os.path.join(BASE_PATH, "images/website_icon.png"))
        DISCLAIMER_TEXT_PATH = os.path.abspath(os.path.join(BASE_PATH, "strings/disclaimer.txt"))
        SIGNATURE_TEMPLATE_PATH = os.path.abspath(os.path.join(BASE_PATH, "templates/signature_template.htm"))
        
        SIGNATURES_PATH = os.path.join(os.path.expanduser("~"), "AppData", "Roaming", "Microsoft", "Signatures")

        # Check if each file exists, and raise an error if it doesn't
        if not os.path.isfile(FONT_PATH):
            raise RuntimeError("Font file doesn't exist")

        if not os.path.isfile(LOGO_PATH):
            raise RuntimeError("Logo file doesn't exist")

        if not os.path.isfile(EMAIL_ICON_PATH):
            raise RuntimeError("Email icon file doesn't exist")

        if not os.path.isfile(PHONE_ICON_PATH):
            raise RuntimeError("Phone icon file doesn't exist")

        if not os.path.isfile(LOCATION_ICON_PATH):
            raise RuntimeError("Location icon file doesn't exist")

        if not os.path.isfile(WEBSITE_ICON_PATH):
            raise RuntimeError("Website icon file doesn't exist")

        if not os.path.isfile(DISCLAIMER_TEXT_PATH):
            raise RuntimeError("Disclaimer text file doesn't exist")

        if not os.path.isfile(SIGNATURE_TEMPLATE_PATH):
            raise RuntimeError("Signature template file doesn't exist")

        user_info = get_user_info()

        sam_account_name = getattr(user_info, 'sAMAccountName', None)
        email = getattr(user_info, 'mail', None)
        
        signature_name = f"{SIGNATURE_NAME} {sam_account_name} ({email})"

        signature_file_path = os.path.join(SIGNATURES_PATH, f"{signature_name}.htm")

        create_signature(
            user_info, 
            SIGNATURE_TEMPLATE_PATH, 
            signature_file_path, 
            FONT_PATH, 
            LOGO_PATH,
            EMAIL_ICON_PATH,
            PHONE_ICON_PATH,
            LOCATION_ICON_PATH,
            WEBSITE_ICON_PATH,
            DISCLAIMER_TEXT_PATH)

        set_signature_in_word(signature_name)

    except Exception as e:
        print(f"An error occurred: {str(e)}")

if __name__ == "__main__":
    main()