import os
import unittest
from unittest.mock import Mock
from main import (
    convert_image_to_base64,
    convert_font_to_base64,
    get_user_info,
)

class TestMain(unittest.TestCase):

    def setUp(self):
        # Set up any necessary variables or paths
        self.BASE_PATH = r'C:\Users\gabriel\Desktop\gitlab\email-signature-template\assets'
        self.FONT_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "fonts/font.ttf"))
        self.LOGO_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "images/logo.png"))
        self.EMAIL_ICON_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "images/email_icon.png"))
        self.PHONE_ICON_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "images/phone_icon.png"))
        self.LOCATION_ICON_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "images/location_icon.png"))
        self.WEBSITE_ICON_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "images/website_icon.png"))
        self.DISCLAIMER_TEXT_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "strings/disclaimer.txt"))
        self.SIGNATURE_TEMPLATE_PATH = os.path.abspath(os.path.join(self.BASE_PATH, "templates/signature_template.htm"))

        self.mock_dispatch = Mock()
        self.mock_dispatch.return_value.UserName = "mock_username"
        
        self.mock_get_object = Mock()
        self.mock_user_info = self.mock_get_object.return_value
        self.mock_user_info.displayName = "Mock User"
        self.mock_user_info.title = "TitleE"
        self.mock_user_info.l = "CityY"
        self.mock_user_info.st = "StateE"
        self.mock_user_info.mail = "email@example.com"
        self.mock_user_info.wWWHomePage = "example.com"
        self.mock_user_info.telephoneNumber = "9999-9999"
        

    def test_convert_image_to_base64(self):
        result = convert_image_to_base64(self.LOGO_PATH)
        self.assertIsInstance(result, str)
        self.assertTrue(result.startswith("data:image/png;base64,"))

    def test_convert_font_to_base64(self):
        result = convert_font_to_base64(self.FONT_PATH)
        self.assertIsInstance(result, str)
        self.assertTrue(result.startswith("data:font/truetype;charset=utf-8;base64,"))
    
    def test_get_user_info(self):
        with unittest.mock.patch("main.win32com.client.Dispatch", self.mock_dispatch):
            with unittest.mock.patch("main.win32com.client.GetObject", self.mock_get_object):
                user_info = get_user_info()

                self.assertEqual(user_info.displayName, "Mock User")
                self.assertEqual(user_info.title, "TitleE")
                self.assertEqual(user_info.l, "CityY")
                self.assertEqual(user_info.st, "StateE")
                self.assertEqual(user_info.mail, "email@example.com")
                self.assertEqual(user_info.wWWHomePage, "example.com")
                self.assertEqual(user_info.telephoneNumber, "9999-9999")
        
        


    # Add more test cases as needed for other functions

if __name__ == "__main__":
    unittest.main()
