# Gerador de Assinatura de E-mail

## Visão Geral
Este script Python automatiza o processo de geração e configuração de assinaturas de e-mail personalizadas para usuários em um ambiente Windows. Ele utiliza o `win32com.client` para interagir com objetos COM do Windows para recuperar informações do usuário do Active Directory e configurar a assinatura no Microsoft Word. O script converte imagens e fontes especificadas para o formato Base64, integra-as em um modelo de assinatura HTML e define a assinatura gerada como padrão para novos e-mails e respostas.

## Recursos
- Recupera informações do usuário do Active Directory
- Converte arquivos de logo e fonte especificados para codificação Base64
- Personaliza um modelo de assinatura HTML com detalhes do usuário
- Define a assinatura gerada como padrão no Microsoft Word

## Pré-requisitos
- Python 3.x
- Módulo `win32com.client`
- Acesso a um ambiente Windows com Active Directory
- Microsoft Word instalado

## Instalação
1. Certifique-se de que o Python 3.x está instalado no seu sistema.
2. Instale o módulo Python necessário:
    ```bash
    pip install -r requirements.txt
    ```
3. Clone este repositório ou baixe o script para a sua máquina local.

## Uso

1. Atualize o script com os caminhos de arquivo apropriados para a fonte, logo e modelo de assinatura HTML.
2. Certifique-se de que o modelo HTML contém espaços reservados para o conteúdo dinâmico (por exemplo, `$font`, `$logo`, `$name`, `$title`, etc.).
3. Execute o script em um prompt de comando ou terminal:
    ```bash
    python main.py.py
    ```
4. O script recuperará as informações do usuário atual, gerará uma assinatura personalizada e a definirá como padrão no Microsoft Word.

## Convertendo para Executável

Para distribuir seu script como um arquivo executável independente no Windows, você pode usar o `PyInstaller`. Isso permite que os usuários executem seu script sem a necessidade de instalar o Python ou quaisquer dependências.

### Pré-requisitos para Conversão
- Instale o `PyInstaller`:
    ```bash
    pip install -r requirements.dev.txt
    ```

### Etapas de Conversão
1. Abra um prompt de comando ou terminal no diretório que contém o seu script.
2. Execute o seguinte comando para criar o executável:
    ```bash
    pyinstaller --noconfirm --onefile --console --name "signature" --hide-console "minimize-early" "main.py"
    ```
    - `--noconfirm`: Sobrescreve quaisquer arquivos existentes criados em execuções anteriores.
    - `--onefile`: Agrupa tudo em um único arquivo executável para simplicidade.
    - `--console`: Este script é executado em uma janela de console.
    - `--name "signature"`: Define o nome do executável de saída para `signature.exe`.
    - `--hide-console`: Oculta a janela do console ao executar a GUI (se aplicável).
    - `main.py`: O caminho para o seu script Python principal.

3. Após a conclusão do processo, encontre seu arquivo `.exe` na pasta `dist` dentro do diretório do seu script.

### Uso do Executável
- Distribua o arquivo `.exe` para os usuários finais.
- Os usuários podem simplesmente dar um duplo clique no executável para executar o script sem nenhuma configuração adicional.

### Notas
- A primeira execução do executável pode demorar um pouco mais, pois extrai os arquivos necessários.
- Se o seu script usa arquivos externos (por exemplo, modelos, imagens), certifique-se de que eles estejam acessíveis ao executável, possivelmente incluindo-os no mesmo diretório.


## Personalização
- Modifique o modelo de assinatura HTML para atender aos seus requisitos de design.
- Atualize os caminhos de arquivo para os arquivos de logo e fonte conforme necessário.

## Solução de Problemas
- Certifique-se de que o script seja executado em um ambiente Windows com acesso ao Active Directory e ao Microsoft Word.
- Verifique os caminhos dos arquivos para garantir que apontem para arquivos válidos no seu sistema.
- Verifique se o modelo HTML usa os espaços reservados corretos para o conteúdo dinâmico.

## Licença
[Licença MIT](LICENSE)

## Aviso
Este script é fornecido como está, sem garantias. Ele é destinado para uso em um ambiente controlado e deve ser testado exaustivamente antes de ser implantado em um ambiente de produção.